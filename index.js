const Discord = require("discord.js");
const client = new Discord.Client();

const WATCH_CHANNELS = {
    "607987379701153826": {
        title: "Welcome to the stream!",
        description: "Don't forget to tune in on Twitch too!\n\nhttps://twitch.tv/luaq",
        url: "https://twitch.tv/luaq",
        author: {
            name: "luaq.#1337",
            url: "https://gitlab.com/Luaq/casual-bot"
        }
    }
};

client.once("ready", () => console.log(`${client.user.tag} has logged in and is now running.`))

client.on("voiceStateUpdate", (oldMember, newMember) => {
    const channelID = newMember.voiceChannelID;
    const embed = WATCH_CHANNELS[channelID];
    if (!embed) {
        return; // the channelID doesn't have an embed
    }
    // send a rich embed message based on which
    // voice channel was joined by member
    newMember.send(new Discord.RichEmbed(embed));
});

client.login(process.env.CASUAL_TOKEN);
